import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  styleUrls: ['./pets.component.css']
})
export class PetsComponent implements OnInit {

  pets: any = [];

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.httpClient.get('https://gmf4bgulq0.execute-api.us-east-2.amazonaws.com/dev/pets')
          .subscribe(response => {
            this.pets = response;
          });
  }

  stringify(obj: any) {
    return JSON.stringify(obj);
  }
}
