import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './Product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url = 'http://localhost:8080/product';

  constructor(private httpClient: HttpClient) { }

  getProducts(): Observable<Product[]> {
    return this.httpClient.get<Product[]>(this.url);
  }

  addProduct(product: Product): Observable<Product[]>{
    return this.httpClient.post<Product[]>(this.url, product);
  }

  removeProduct(id: number): Observable<Product[]>{
    return this.httpClient.delete<Product[]>(this.url + "/" + id);
  }
}
