import { Component, OnInit } from '@angular/core';
import { ProductService} from './product-service.service';
import { Product } from './Product';


@Component({
  selector: 'product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  products: Product[] = [];
  product: Product = new Product();

  constructor(private productservice: ProductService) { }

  ngOnInit(): void {
    this.productservice.getProducts().subscribe(products => {
      this.products = products;
    });
  }

  stringify(obj: any) {
    return JSON.stringify(obj);
  }

  addProduct(){        
    this.productservice.addProduct(this.product).subscribe(products => {
      this.products = products;
    });
  }

  removeProduct(id?: number){
    if(!id) return;
    this.productservice.removeProduct(id).subscribe(products => {
      this.products = products;
    });
  }
}
